Docker image for Rails development (_rails 4.2.1_, _ruby 2.2.1_). Rails is installed
on user level including _ruby_ and _rvm_.

## Build

```
make docker
```

## Bootstrap

```
make setup

# update your database config

make setup-db
```

## Run

```
docker run
```