all:

docker:
	docker-compose pull db
	docker-compose build web

setup:
	docker-compose run web /bin/bash -l -c "bundle config --local path vendor/bundle"
	docker-compose run web /bin/bash -l -c "rails new ."

run:
	docker-compose up web

clean:
	docker-compose rm web db