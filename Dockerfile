FROM debian:8.1
MAINTAINER Gregor Mazovec <gregor.mazovec@gmail.com>

# Additional packages required for RVM user level installation
RUN apt-get update && apt-get install -y \
      curl build-essential patch gawk g++ gcc make libc6-dev patch \
      libreadline6-dev zlib1g-dev libssl-dev libyaml-dev libsqlite3-dev \
      sqlite3 autoconf libgdbm-dev libncurses5-dev automake libtool \
      bison pkg-config libffi-dev

# Packages for Rails
RUN apt-get install -y \
      nodejs libpq-dev

# Image user
RUN useradd -ms /bin/bash rubist
USER rubist

# Config
ENV RUBY_VERSION 2.2
ENV RAILS_VERSION 4.2.1

# rvm installation
RUN gpg --keyserver hkp://keys.gnupg.net --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3
RUN /bin/bash -l -c "curl -sSL https://get.rvm.io | bash -s stable --ruby"
RUN /bin/bash -l -c "rvm install $RUBY_VERSION"
RUN /bin/bash -l -c "rvm use $RUBY_VERSION"

# rails installation
RUN /bin/bash -l -c "echo 'gem: --no-ri --no-rdoc' > ~/.gemrc"
RUN /bin/bash -l -c "gem install bundler --no-ri --no-rdoc"
RUN /bin/bash -l -c "gem install rails -v $RAILS_VERSION --no-ri --no-rdoc"

# after installation configuration
ONBUILD RUN /bin/bash -l -c "source /home/$USER/.rvm/scripts/rvm"